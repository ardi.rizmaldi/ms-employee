export const mockGetEmployees = jest.fn();
export const mockCreateEmployee = jest.fn();
export const mockUpdateEmployee = jest.fn();
export const mockDeleteEmployee = jest.fn();
const mock = jest.fn().mockImplementation(() => {
  return {
    getEmployees: mockGetEmployees,
    createEmployee: mockCreateEmployee,
    updateEmployee: mockUpdateEmployee,
    deleteEmployee: mockDeleteEmployee
  };
});

export default mock;
