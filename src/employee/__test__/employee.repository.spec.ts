import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

import { EmployeeModel } from '../employee.model';
import employeeRepository from '../employee.repository';

jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

describe('employee.repository', () => {
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, { useNewUrlParser: true });
  });

  afterAll(async () => {
    mongoose.disconnect();
    mongod.stop();
  });

  describe('get', () => {
    it('should get all employee', async () => {
      await EmployeeModel.create(
        {
          name: 'u1',
          age: 26,
          adress: 'anagram road'
        },
        {
          name: 'u2',
          age: 30,
          adress: 'pangram road'
        }
      );

      const employees = await employeeRepository.get();
      expect(employees).toHaveLength(2);
    });
  });

  describe('create', () => {
    it('should create new employee', async () => {
      const newEmployee = await employeeRepository.create({
        name: 'new employee',
        age: 40,
        adress: 'isgoram road'
      });
      expect(newEmployee.id).toBeDefined();
    });
  });

  describe('getFirstByName', () => {
    it('should return first employee if name exists in db', async () => {
      await EmployeeModel.create({
        name: 'exist name',
        age: 30,
        adress: 'pangram road'
      });
      const employee = await employeeRepository.getFirstByName('exist name');
      expect(employee.name).toEqual('exist name');
    });

    it('should return null if name not exists in db', async () => {
      const employee = await employeeRepository.getFirstByName('not exist name');
      expect(employee).toBeNull();
    });
  });

    describe('UpdateEmployee', () => {
    it('should update data employee', async () => {
        await EmployeeModel.create({
            name: 'Anagram',
            age: 25,
            adress: 'Asteroid road no 4'
        });

        const oldEmployee = await EmployeeModel.findOne({ name: 'Anagram' });

        const updatedEmployee = await employeeRepository.updateById(
            oldEmployee.id,
            {
                name: 'stepen',
                age: 40,
                adress: 'Dayeuhkolot'
            }
        );

        expect(updatedEmployee.name).toEqual('stepen');
        expect(updatedEmployee.age).toEqual(40);
        expect(updatedEmployee.adress).toEqual('Dayeuhkolot')
    });    
    });

    describe('DeleteEmployee', () => {
    it('should delete employee data', async () => {
        await EmployeeModel.create ({
            name: 'pangram',
            age: 23,
            adress: 'isogram road no 412'
        });
        
        const oldEmployee = await EmployeeModel.findOne({name: 'pangram'});

        const deletedEmployee = await employeeRepository.deleteById(oldEmployee.id);

        expect(await EmployeeModel.findById(oldEmployee.id)).toBeNull();

        expect(deletedEmployee.name).toEqual('u1');
        expect(deletedEmployee.age).toEqual(23);
        expect(deletedEmployee.adress).toEqual(20);
    });
    });
});
