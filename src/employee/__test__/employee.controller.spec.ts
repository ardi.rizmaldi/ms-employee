import hapi = require('@hapi/hapi');

import employeeService from '../employee.service';
import employeeController from '../employee.controller';
import { IEmployee } from '../employee.interface';
jest.mock('../employee.service', () => ({
  getEmployees: jest.fn(),
  createEmployee: jest.fn(),
  updateEmployee: jest.fn(),
  deleteEmployee: jest.fn()
}));
let server: hapi.Server;

describe('employee.controller', () => {
  beforeAll(async () => {
    server = new hapi.Server();
    server.route(employeeController);
  });

  describe('GET /employees', () => {
    it('should return employeeService getEmployees with code 200', async () => {
      const testEmployees = [
        {
          _id: '5d29b6716394dea3588023d4',
          name: 'mahesh',
          age: 28,
          adress: 'pangram',
          __v: 0,
          id: '5d29b6716394dea3588023d4'
        }
      ];
      employeeService.getEmployees.mockResolvedValueOnce(testEmployees);
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'GET',
        url: `/employees`
      });
      expect(result.statusCode).toBe(200);
      expect(result.result).toEqual(testEmployees);
      expect(employeeService.getEmployees).toHaveBeenCalledTimes(1);
    });
  });

  describe('POST /employees', () => {
    it('should return error on empty name', async () => {
      const testEmployee: IEmployee = {
        name: 'selenoid',
        age: 20,
        adress: 'pangram road'
      };
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(result.statusCode).toBe(400);
      expect(result.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return error on not enough age', async () => {
      const testEmployee: IEmployee = {
        name: 'test',
        age: 10,
        adress: 'pangram road'
      };
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(response.statusCode).toBe(400);
      expect(response.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return 201 on valid payload and success service call', async () => {
      const testEmployee: IEmployee = {
        name: 'test',
        age: 38,
        adress: 'pangram'
      };

      employeeService.createEmployee.mockResolvedValueOnce({
        ...testEmployee,
        id: '123id'
      });

      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(response.statusCode).toBe(201);
      expect(response.result).toEqual({
        ...testEmployee,
        id: '123id'
      });
    });
  });

  describe('PUT /employees/{id}', () => {
    it('should update employee', async () => {
        const testEmployee = {
            id: '123id',
            name: 'pangram',
            age: 30,
            adress: 'anagram road',
            _id: '123id',
            __v: 0
        };
        const newEmployee: IEmployee = {
          name: 'anagram update',
          age: 32,
          adress: 'angram road update',
        };
        employeeService.updateEmployee.mockResolvedValueOnce({
            _id: testEmployee._id,
            ...newEmployee,
            id: testEmployee.id,
            __v: testEmployee.__v
        });
        const result: hapi.ServerInjectResponse = await server.inject({
            method: 'PUT',
            url: `/employees/`+ testEmployee.id,
            payload: newEmployee,
        });
        expect(result.result).toEqual({
            ...newEmployee,
            id: '123id',
            __v: 0,
            _id: '123id'
        });
        expect(result.statusCode).toBe(201);
    });
  });

  describe('DELETE /employees/{id}', () => {
    it('should delete employee', async () => {
        const testEmployee = {
            name: 'anagram',
            age: 25,
            adress: 'isogram',
            id: '5e5b3cc3711ad904b9f19061',
            __v: 0,
            _id: '5e5b3cc3711ad904b9f19061'
        };
        employeeService.deleteEmployee.mockResolvedValueOnce({
            ...testEmployee,
        });
        const result: hapi.ServerInjectResponse = await server.inject({
          method: 'DELETE',
          url: `/employees/` + testEmployee.id,
          payload: testEmployee
        });
        expect(result.statusCode).toBe(200);
    }); 
  });
});
