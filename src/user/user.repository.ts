import { UserModel, UserDocument } from './user.model';
import { IUser, IUserResponse } from './user.interface';

const userDocumentToObject = (document: UserDocument) =>
  document.toObject({ getters: true }) as IUserResponse;

const userDocumentsToObjects = (documents: UserDocument[]) =>
  documents.map(userDocumentToObject);

const get = async () => {
  const documents = await UserModel.find().exec();
  return userDocumentsToObjects(documents);
};

const create = async (user: IUser) => {
  const newUser = new UserModel(user);

  await newUser.save();

  return userDocumentToObject(newUser);
};

const getFirstByName = async (name: string) => {
  const user = await UserModel.findOne({ name }).exec();
  return user && userDocumentToObject(user);
};

const userRepository = {
  get,
  create,
  getFirstByName
};
export default userRepository;
