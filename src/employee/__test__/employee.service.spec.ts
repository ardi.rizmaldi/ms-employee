import employeeService from '../employee.service';
import employeeRepository from '../employee.repository';
import { ERROR_CODE } from '../../common/errors';
import { AppError } from '../../errors/AppError';

jest.mock('../employee.repository');

describe('employeeService', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('getEmployees', () => {
    it('should return repository get result', async () => {
      const employeesTest = [
        {
          name: 'test'
        }
      ];
      employeeRepository.get.mockResolvedValueOnce(employeesTest);
      const employees = await employeeService.getEmployees();
      expect(employees).toEqual(employeesTest);
    });
  });

  describe('createEmployee', () => {
    it('should throw error if repository find existing employee with same name', async () => {
      const employeeInfo = {
        name: 'test',
        age: 12
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce({
        name: 'test'
      });
      try {
        await employeeService.createEmployee(employeeInfo);
      } catch (e) {
        expect(e).toBeInstanceOf(AppError);
        expect(e.errorCode).toEqual(ERROR_CODE.USER_NAME_EXISTED);
      } finally {
        expect(employeeRepository.create).not.toBeCalled();
      }
    });

    it('should call repository create employee', async () => {
      const employeeInfo = {
        name: 'test',
        age: 12
      };
      employeeRepository.getFirstByName.mockResolvedValueOnce(null);
      await employeeService.createEmployee(employeeInfo);
      expect(employeeRepository.create).toBeCalledWith(employeeInfo);
    });
  });


  describe('updateEmployee', () => {
    it('should call repository update by id', async () => {
        const employeeInfo = {
            name: 'anagram',
            age: 42,
            adress: 'panagram road'
        };
        employeeRepository.updateById.mockResolvedValueOnce(employeeInfo);
        const updateById = await employeeService.updateEmployee('123id', employeeInfo);
        expect(employeeRepository.updateById).toBeCalledWith(
            '123id',
            employeeInfo
        );
        expect(updateById).toEqual(employeeInfo);
    });
});

describe('deleteEmployee', () => {
    it('should call repository delete by id', async () => {
      employeeRepository.deleteById.mockResolvedValueOnce(null);
      await employeeService.deleteEmployee('123id');
      expect(employeeRepository.deleteById).toBeCalledWith('123id');
    });
});
});
