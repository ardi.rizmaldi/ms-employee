import { ERROR_CODE } from '../common/errors';

import employeeRepository from './employee.repository';
import { IEmployee } from './employee.interface';
import { AppError } from '../errors/AppError';

const getEmployees = () => {
  return employeeRepository.get();
};

const createEmployee = async (employee: IEmployee) => {
  const existingEmployee = await employeeRepository.getFirstByName(employee.name);
  if (existingEmployee) {
    throw new AppError(ERROR_CODE.USER_NAME_EXISTED);
  }
  return employeeRepository.create(employee);
};

const updateEmployee = async (id: string, employee: IEmployee) => {
  return employeeRepository.updateById(id, employee);
};

const deleteEmployee = async (id: string) => {
  return employeeRepository.deleteById(id);
};

const employeeService = {
  getEmployees,
  createEmployee,
  deleteEmployee,
  updateEmployee
};
export default employeeService;
