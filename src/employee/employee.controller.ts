import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import employeeService from './employee.service';
import {
  EmployeeListResponseValidator,
  createEmployeeRequestValidator,
  EmployeeResponseValidator,
  updateEmployeeRequestValidator,
  ParamsValidator
} from './employee.validator';
import { IEmployeeRequest } from './employee.interface';

const getEmployee: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/employees',
  options: {
    description: 'Get the list of employees',
    notes: 'Get the list of employees',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeListResponseValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employees retrieved'
          }
        }
      }
    }
  }
};

const createEmployee: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/employees',
  options: {
    description: 'Create new employee',
    notes: 'All information must valid',
    validate: {
      payload: createEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newEmployee = await employeeService.createEmployee(hapiRequest.payload);
      return hapiResponse.response(newEmployee).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee created.'
          }
        }
      }
    }
  }
};

const updateEmployee: hapi.ServerRoute = {
  method: Http.Method.PUT,
  path: '/employees/{id}',
  options: {
    description: 'Update employee by id',
    notes: 'All information must valid',
    validate: {
      payload: updateEmployeeRequestValidator,
      params: ParamsValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const updateEmployee = 
      (await employeeService.updateEmployee(
        hapiRequest.params.id,
        hapiRequest.payload)) || 'Not Found';
      return hapiResponse.response(updateEmployee).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employee Data Updated.'
          },
        }
      }
    }
  }
};

const deleteEmployee: hapi.ServerRoute = {
  method: Http.Method.DELETE,
  path: '/employees/{id}',
  options: {
    description: 'Remove employee by id',
    notes: 'All information must valid',
    validate: {
      params: ParamsValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const deleteEmployee = 
      (await employeeService.deleteEmployee(hapiRequest.params.id)) || 'ID Not Found';
      return hapiResponse.response(deleteEmployee).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employee removed.'
          }
        }
      }
    }
  }
};

const employeeController: hapi.ServerRoute[] = [getEmployee, createEmployee, updateEmployee, deleteEmployee];
export default employeeController;
