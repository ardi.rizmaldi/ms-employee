import * as Joi from '@hapi/joi';

import EmployeeConstant from './employee.constant';

import { MongooseBase } from '../common/validators';

const EmployeeValidator = {
  name: Joi.string()
    .trim()
    .required(),
  age: Joi.number()
    .min(EmployeeConstant.MIN_EMPLOYEE_AGE)
    .required(),
  adress: Joi.string().trim(),
};

const UpdateEmployeeValidator = {
    name: Joi.string()
      .trim()
      .required(),
    age: Joi.number()
      .min(EmployeeConstant.MIN_EMPLOYEE_AGE)
      .required(),
    adress: Joi.string().trim(),
 };

 const RemoveEmployeeValidator = {
  id: Joi.string()
    .trim()
    .required(),
};

const ParamsValidator = {
  id: Joi.string()
    .trim()
    .required(),
};

const EmployeeResponseValidator = Joi.object({
  ...MongooseBase,
  ...EmployeeValidator
})
  .required()
  .label('Response - Employee');

const EmployeeListResponseValidator = Joi.array()
  .items(EmployeeResponseValidator)
  .label('Response - Employees');

const createEmployeeRequestValidator = Joi.object({ ...EmployeeValidator }).label(
  'Request - new employee'
);

const updateEmployeeRequestValidator = Joi.object({ ...UpdateEmployeeValidator }).label(
  'Request - update data employee'
);

const deleteEmployeeRequestValidator = Joi.object({ ...RemoveEmployeeValidator }).label(
  'Request - remove data employee'
);

export {
  EmployeeResponseValidator,
  createEmployeeRequestValidator,
  updateEmployeeRequestValidator,
  deleteEmployeeRequestValidator,
  EmployeeListResponseValidator,
  EmployeeValidator,
  ParamsValidator
};
